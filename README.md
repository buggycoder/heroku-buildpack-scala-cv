Heroku buildpack: Scala + OpenCV
=========================

Combines code from the official [Heroku buildpack for Scala](https://github.com/heroku/heroku-buildpack-scala) and [OpenCV buildpack](https://github.com/hdachev/heroku-buildpack-opencv).


Prerequisites
-------

heroku config:set OPENCV_LUCID_BIN=< URL for compressed .tar.gz containing opencv bin libs built for Ubuntu 10.04 Lucid Lynx >
